//
//  ViewController.swift
//  CSNumberPad
//
//  Created by iamaunz on 12/15/2558 BE.
//  Copyright © 2558 iamaunz. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let sc = UIScreen.mainScreen().bounds.size
        let numberPad = CSNumberPad(frame: CGRectMake((sc.width-300)/2, sc.height-280, 300, 280))
        self.view.addSubview(numberPad)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

