//
//  CSNumberPad.swift
//  CSNumberPad
//
//  Created by iamaunz on 12/15/2558 BE.
//  Copyright © 2558 iamaunz. All rights reserved.
//

import UIKit
import Foundation

public class CSNumberPad :UIView
{
    var sc          = CGSizeZero
    
    convenience init () {
        self.init(frame: CGRect.zero)
    }
    
    override public init(frame: CGRect) {
        super.init(frame: CGRectMake(frame.origin.x, frame.origin.y, 300, frame.height))
        self.sc = UIScreen.mainScreen().bounds.size
        self.setNumberPad()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init(coder:) has not been implemented")
    }
    
    func getButton(title :String, frame :CGRect) -> UIButton {
        let button = UIButton(frame: frame)
        button.setTitleColor(UIColor.lightGrayColor(), forState: UIControlState.Highlighted)
        button.setTitleColor(UIColor.darkGrayColor(), forState: UIControlState.Normal)
        button.titleLabel?.font = UIFont(name: "Helvetica", size: 32)
        button.setTitle(title, forState: UIControlState.Normal)
        
        let line = UIView(frame: CGRectMake(10, frame.height, frame.width-20, 0.5))
        line.backgroundColor = UIColor.lightGrayColor()
        button.addSubview(line)
        
        return button
    }
    
    func setNumberPad() {
        let height = self.frame.height/4
        self.addSubview(getButton("1", frame: CGRectMake(0, (self.frame.height/4*0), 100, height)))
        self.addSubview(getButton("2", frame: CGRectMake(100, (self.frame.height/4*0), 100, height)))
        self.addSubview(getButton("3", frame: CGRectMake(200, (self.frame.height/4*0), 100, height)))
        self.addSubview(getButton("4", frame: CGRectMake(0, (self.frame.height/4*1), 100, height)))
        self.addSubview(getButton("5", frame: CGRectMake(100, (self.frame.height/4*1), 100, height)))
        self.addSubview(getButton("6", frame: CGRectMake(200, (self.frame.height/4*1), 100, height)))
        self.addSubview(getButton("7", frame: CGRectMake(0, (self.frame.height/4*2), 100, height)))
        self.addSubview(getButton("8", frame: CGRectMake(100, (self.frame.height/4*2), 100, height)))
        self.addSubview(getButton("9", frame: CGRectMake(200, (self.frame.height/4*2), 100, height)))
        self.addSubview(getButton(".", frame: CGRectMake(0, (self.frame.height/4*3), 100, height)))
        self.addSubview(getButton("0", frame: CGRectMake(100, (self.frame.height/4*3), 100, height)))
        self.addSubview(getButton("D", frame: CGRectMake(200, (self.frame.height/4*3), 100, height)))
    }
    
    func diplayDot() {
        
    }
    
}





